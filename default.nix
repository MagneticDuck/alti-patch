{ nixpkgs ? import <nixpkgs> {} }:

let
  java = import ./java.nix { inherit nixpkgs; };

  jar = ./altitude/game.jar;
  mkServer = patch:
    let
      injection = java.compile { inherit jar; source = patch; };
    in
      java.inject { inherit jar injection; };

in

  rec {
    ## patched servers -- use one of these in place of game.jar for fun and profit
    whitelist = mkServer ./patches/whitelist;
    deny-entry = mkServer ./patches/deny-entry;

    ## decompiled game.jar for reference
    decompiled = java.decompile { unpacked = java.unpack { inherit jar; }; };

    ## compile the 'test' patch
    compiled = java.compile { inherit jar; source = ./patches/test; };

  }
