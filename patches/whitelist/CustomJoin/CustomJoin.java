package CustomJoin;

import java.io.*;

public class CustomJoin {
    // Return null to allow the join, otherwise return the error
    // message to be sent to the player as a string.
    public static String checkJoin(JoinReqInfo info, int port){
        File f = new File("allow_all_" + port);
        if(f.exists()) return null;

        try {
            FileInputStream file = new FileInputStream("whitelist_" + port);
            BufferedReader br = new BufferedReader(new InputStreamReader(file));
            String id;
            while ((id = br.readLine()) != null) {
                 if(info.vaporID.toString().equals(id))
                     return null;
            }
            br.close();
        } catch(IOException e){
            System.out.println("Error reading whitelist: " + e.toString());
            return "Server Error";
        }
        return "Only whitelisted players can join this server right now, " +
                "sorry!";
    }
}
