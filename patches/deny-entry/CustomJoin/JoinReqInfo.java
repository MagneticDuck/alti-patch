package CustomJoin;

import java.util.UUID;

public class JoinReqInfo {
   public String nick;
   public UUID vaporID;
   public String password;
   public int ace = 0;
   public int level = 1;
   public int kills = 0;
   public int deaths = 0;
}
