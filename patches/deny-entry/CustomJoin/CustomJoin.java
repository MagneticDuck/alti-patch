package CustomJoin;

import java.io.*;

public class CustomJoin {
    // Return null to allow the join, otherwise return the error
    // message to be sent to the player as a string.
    public static String checkJoin(JoinReqInfo info, int port){
        if (info.nick.equals("Magnetic Duck")
            || info.nick.equals("v x.x")) {
            return null;
        } else {
            return "Sorry, you're not cool enough to join this server.";
        }
    }
}
