import java.util.UUID;

import CustomJoin.CustomJoin;
import CustomJoin.JoinReqInfo;
import org.apache.log4j.Logger;

public class jj extends wr {
   private static final Logger a = Logger.getLogger(jj.class);
   private fN this_b;
   private yS c = new yS(16000.0F);
   // $FF: synthetic field
   private static int[] d;

   public jj(fN var1) {
      this.this_b = var1;
   }

   public void a(DZ var1, int var2, qm var3, Ui var4) {
      try {
         gk var5 = UY.a(Za.b);
         var5.b(2, var2);
         qm.b.a(var3, var5);
         var4.a((nu)var5);
         this.a(var1, zq.c, var5);
      } catch (Exception var6) {
         a.error(var6, var6);
      }

   }

   public void a(DZ var1, String var2) {
      try {
         gk var3 = UY.a(Za.c);
         var3.a(var2);
         this.a(var1, zq.c, var3);
      } catch (Exception var4) {
         a.error(var4, var4);
      }

   }

   public void a(DZ var1) {
      try {
         gk var2 = UY.a(Za.d);
         this.a(var1, zq.b, var2);
      } catch (Exception var3) {
         a.error(var3, var3);
      }

   }

   public void a(DC validation) {
      try {
         lY id = validation.a();
         if(this.this_b.l().a(id.D())) {
            Jt var3;
            if(validation.b() && validation.c() && validation.h()) {
               var3 = this.this_b.l().b(id.D());
               this.this_b.l().a(var3, id.E(), validation);
            } else {
               var3 = this.this_b.l().b(id.D());
               a.info("Validation booting " + var3 + "; reason: " + validation.f());
               this.this_b.l().a(var3, "Validation failed: " + validation.f(), "connection lost");
            }
         } else {
            eE reqInfo = (eE)this.c.a(id.f());
            if(reqInfo == null) {
               a.info("Ignoring join request: no valid context for " + id);
               return;
            }

            if(!ty.a((Object) reqInfo.d, (Object) id.b())) {
               a.info("Remapping " + id.b() + " to " + reqInfo.d);
               id.a((DZ) reqInfo.d);
            }

            reqInfo.a = id;
            //String failReason = this.a(reqInfo);
            String failReason = CustomJoin.checkJoin(makeJoinReqInfo(reqInfo),
                    this.this_b.m().a());

            if(id.a()) { //if id.isLoggedIn()
               if(failReason != null) {
                  this.a(reqInfo, false, failReason, (DC)null); // this.join fail
               } else {
                  if (validation.b() && validation.c() && validation.h()) {
                    this.a(reqInfo, true, null, validation);
                  } else {
                     this.a(reqInfo, false, validation.f(), validation);
                  }
               }
            }
         }
      } catch (Exception var5) {
         a.error(var5, var5);
      }

   }

   public void a(eE var1, boolean var2, String var3, DC var4) {
      lY var5 = var1.a;
      UUID var6 = var5.D();
      DZ var7 = var5.b();
      if(!mF.a.equals(var6)) {
         if(this.this_b.l().a(var6)) {
            a.info(var7 + " attempted to join while already connected; disconnecting");
            this.this_b.l().a((Jt)this.this_b.l().b(var6), (String)null, (String)null);
         } else if(this.this_b.l().a(var7)) {
            a.info(var7 + " attempted to join while already connected; disconnecting");
            this.this_b.l().a((Jt)this.this_b.l().b(var7), (String)null, (String)null);
         }
      }

      gk var8 = UY.a(Za.a);
      var8.a(var2);
      if(!var2) {
         var8.a(var3);
      }

      this.a(var7, zq.c, var8);
      if(var2) {
         this.this_b.l().a(var5, var4, var1.e);
      }

   }

   private String a(eE var1) {
      long var2 = System.currentTimeMillis();
      long var4 = this.this_b.m().R().a(var1.a);
      if(var4 > var2) {
         long var12 = var4 - var2;
         String var14 = this.this_b.m().R().b(var1.a);
         var14 = var14 + "\n\nThis ban expires in " + me.a(var12, true) + ".\n\nOnly the player hosting this server can repeal your ban.\nNimbly Games is not affiliated with this server.";
         return var14;
      } else {
         String var13;
         if(!var1.d.k()) {
            String var6 = this.this_b.m().p();
            if(var6 != null && var6.length() > 0 && !ty.a((Object)var6, (Object)var1.c)) {
               if(var1.c.length() == 0) {
                  var13 = "Private Server requires Secret Code";
               } else {
                  var13 = "Incorrect Secret Code";
               }

               return var13;
            }

            int var7 = ql.a(var1.e.a(), var1.e.b()).c();
            int var8 = this.this_b.m().n();
            if(var8 != 0 && var7 < var8) {
               String var15 = "To join this server you must be level " + var8 + " or higher.";
               var15 = var15 + "\nYou are level " + var7 + ".";
               return var15;
            }

            int var9 = this.this_b.m().o();
            if(var9 != 0 && var7 > var9) {
               String var10 = "To join this server you must be level " + var9 + " or lower.";
               var10 = var10 + "\nYou are level " + var7 + ".";
               return var10;
            }
         }

         PK var11 = this.this_b.l().n().C();
         if(!var1.b.equals(var11)) {
            if(var1.b.a(var11)) {
               var13 = "Server is running a newer version of Altitude.\n\nRestart Altitude to update.";
            } else {
               var13 = "Server is running an outdated version of Altitude.";
            }

            return var13;
         } else {
            return !this.this_b.l().a(var1.a)?"Server is full":null;
         }
      }
   }

   private void b(DZ var1, LG var2) {
      PK var3 = this.this_b.l().n().C();
      PK var4 = null;
      String var5 = null;
      new b();
      lY var7 = null;

      b var6;
      String failReason;
      boolean var10;
      try {
         var4 = PK.a(var2.j());
         var5 = var2.j();
         var6 = (b)b.a.b(var2);
         var7 = (lY)Sj.d.b(var2);
      } catch (Exception var12) {
         a.info("Failed to parse join request from " + var1);
         if(var4 != null && !var4.a(var3)) {
            failReason = "Server is running an outdated version of Altitude.";
         } else {
            failReason = "Server is running a newer version of Altitude.\n\nRestart Altitude to update.";
         }

         var10 = false;
         gk var11 = UY.a(Za.a);
         var11.a(var10);
         var11.a(failReason);
         this.a(var1, zq.c, var11);
         return;
      }

      a.info("Handling join request from " + var1 + " -> " + var7);
      if(!var1.equals(var7.b())) {
         a.info("join request IP mismatch; packetIp=" + var1 + " vs vaporIp=" + var7.b());
         var7.a((DZ)var1);
      }

      eE reqInfo = new eE(this, var7, var4, var5, var1, var6); //join req info
      //failReason = this.a(reqInfo);
      failReason = CustomJoin.checkJoin(makeJoinReqInfo(reqInfo),
              this.this_b.m().a());
      if(failReason != null) {
         this.a(reqInfo, false, failReason, (DC)null);
      } else {
         this.c.a(var7.f(), reqInfo);
         var10 = false;
         if(!var1.k() && (!var10 || !ty.a((Object)var1.g(), (Object)DZ.b))) {
            if(!mF.a.equals(var7.D())) {
               if(this.this_b.l().a(var7.D())) {
                  a.info(var7 + " attempted to join while already connected; disconnecting");
                  this.this_b.l().a((Jt)this.this_b.l().b(var7.D()), (String)null, (String)null);
               } else if(this.this_b.l().a(var7.b())) {
                  a.info(var7 + " attempted to join while already connected; disconnecting");
                  this.this_b.l().a((Jt)this.this_b.l().b(var7.b()), (String)null, (String)null);
               }
            }

            this.this_b.d().a(var7);
         } else {
            this.a(reqInfo, true, (String)null, (DC)null);
         }

      }
   }

   private static JoinReqInfo makeJoinReqInfo(eE o){
      JoinReqInfo ret = new JoinReqInfo();
      ret.vaporID = o.a.D();
      ret.nick = o.a.F();
      ret.password = o.c;
      ret.ace = o.e.a();
      ret.level = o.e.b();
      ret.kills = o.e.c();
      ret.deaths = o.e.d();
      return ret;
   }

   public void a(float var1) {
      this.c.a(var1);
   }

   public void a(DZ var1, LG var2) {
      try {
         switch(a()[UY.a((nQ)var2).ordinal()]) {
         case 1:
            this.b(var1, var2);
            break;
         case 2:
            qm var3 = (qm)qm.b.b(var2);
            this.this_b.l().a(var1, var3);
            break;
         case 3:
            if(this.this_b.l().a(var1)) {
               Jt var4 = this.this_b.l().b(var1);
               a.info("Server got disconnect request from " + var4);
               this.this_b.l().a((Jt)var4, (String)null, (String)null);
            }
            break;
         case 4:
            if(this.this_b.l().a(var1)) {
               this.this_b.l().b(var1).j();
            }
         }
      } catch (Exception var5) {
         a.error(var5, var5);
      }

   }

   // $FF: synthetic method
   static int[] a() {
      int[] var10000 = d;
      if(d != null) {
         return var10000;
      } else {
         int[] var0 = new int[Za.values().length];

         try {
            var0[Za.c.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            var0[Za.a.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[Za.d.ordinal()] = 4;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            var0[Za.b.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

         d = var0;
         return var0;
      }
   }

   static class eE {
      lY a;
      PK b;
      String c;
      DZ d;
      b e;
      // $FF: synthetic field
      final jj f;

      private eE(jj var1, lY var2, PK var3, String var4, DZ var5, b var6) {
         this.f = var1;
         this.a = var2;
         this.b = var3;
         this.c = var4;
         this.d = var5;
         this.e = var6;
      }
   }
}
