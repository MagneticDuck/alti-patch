{ nixpkgs }:

with nixpkgs;

# Here we define a set of utilities for modifying java applications.

let

  # Fernflower java decompiler from github.
  fernflower = stdenv.mkDerivation {
    name = "fernflower";
    phases = "installPhase";
    src = fetchFromGitHub {
      owner = "fesh0r";
      repo = "fernflower";
      rev = "9d5a863e4d63b62c8041d034b23b7425e5cec620";
      sha256 = "0hffxnmbc2ld9rwda0dizs74wyykql50cz9mzkpm4i02pgz7bqsw";
    };
    buildInputs = [ant openjdk8];
    installPhase = ''
      cp -r $src/* . ;
      ant;
      cp fernflower.jar $out ;
    '';
  };

  # Empty directory, to serve as the null injection.
  empty = runCommand "empty" {} "mkdir -p $out";

in

rec {
  # Unpack a jar file, resulting in a bunch of .class files.
  unpack =
    { jar # The .jar file to unpack.
    } :
      runCommand "unpacked" {
        buildInputs = [openjdk8]; 
      }
      ''
        echo UNPACKING...

        mkdir -p wrap; cd wrap;
        cp ${jar} ./file.jar;
        jar xf file.jar;
        rm file.jar;
        mkdir -p $out;
        cp -r * $out/;
      '';

  # Decompile a directory full of class files into a set of files that
  # we can build custom classes against.
  decompile =
    { unpacked # The unpacked directory to decompile.
    } :
      runCommand "decompiled" {
        buildInputs = [openjdk8];
      }
      ''
        echo DECOMPILING...

        mkdir -p wrap; cd wrap;
        mkdir -p $out/ ;
        cp -r ${unpacked}/* . ;
        java -jar ${fernflower} . $out/ ;
      '';

  # Compile custom source into injectable .classes.
  compile =
    { jar    # Game .jar to compile against.
    , source # Directory of custom files to compile.
    } :
      let
        unpacked = unpack { inherit jar; };
        command =
        ''
          echo COMPILING...
          mkdir -p compiled/
          javac -cp ${./libs}/alti/log4j-1.2.15.jar:${jar} -d "compiled" $(find ${source} -type f)
          mkdir -p $out/
          cp -r ./compiled/* $out/
        '';
      in
        runCommand "compiled" {
          buildInputs = [openjdk7];
        }
        ''
          echo "${command}"
          ${command}
        '';

  # Inject a directory of .classes into a jar.
  inject =
    { jar,              # Base file to inject into.
      injection ? empty # Directory of classes to inject.
    }:
      runCommand "injected" {
        buildInputs = [p7zip];
      }
      ''
        echo INJECTING...
        cp ${jar} file.jar
        for file in $(find ${injection})
        do
          echo INJECTING $file...
          7z a file.jar $file &> /dev/null
        done
        cp file.jar $out
      '';

}

